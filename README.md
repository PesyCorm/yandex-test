# Yandex Test  
Отчет можно посмотреть [здесь](https://pesycorm.gitlab.io/yandex-test/)  
_Подготовлено для автозапуска на gitlab-ci_

**Для запуска локально:**

1. Скопировать репозиторий
1. Установить зависимости из requirements.txt  

- Для запуска с формированием отчета pytest-html использовать команду `pytest --html=report.html`  

- Чтобы получить allure-report:  
_Для запуска приложения командной строки должна быть установлена ​​среда выполнения Java_  
-Скачать и распаковать allure из https://github.com/allure-framework/allure2/releases/tag/2.13.9 или воспользовавшись инструкцией https://docs.qameta.io/allure/#_get_started  
-Запустить тесты командой `pytest --alluredir=./protocol/allure_folder`  
-Затем сформировать отчет командой `<путь до загруженной папки>/allure-2.13.9/bin/allure serve ./protocol/allure_folder -o ./protocol/allure-report`
