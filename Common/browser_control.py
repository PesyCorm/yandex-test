from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


def switch_window(driver, window_num):
    driver.switch_to.window(WebDriverWait(driver, 10).until(lambda driver: driver.window_handles[window_num]))
