from selenium.webdriver.common.by import By
from .BasePage import BasePage


class StartPage(BasePage):

    def click_on_yandex_service(self, requested_service: str):

        services_list = self.waitAll(By.CLASS_NAME, "services-new__item-title")

        for el in services_list:
            if requested_service in el.text:
                el.click()
