from .YandexStartPage import StartPage
from .YandexResultsPage import ResultsPage
from .YandexImageMain import ImagePage
from .CommonElements import CommonElements