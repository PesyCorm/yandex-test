from selenium.webdriver.common.by import By
from .BasePage import BasePage


class ResultsPage(BasePage):

    def return_result_list(self, no_ad=True):

        result_list_len = len(self.waitAll(By.CLASS_NAME, "organic__url-text"))

        result_list_value = []

        for el in range(result_list_len):
            result_list_value.append(
                {"result_text": self.waitAll(By.CLASS_NAME, "organic__url-text")[el].text,
                 "result_link": self.waitAll(By.CSS_SELECTOR, ".path__item")[el].get_attribute("href"),
                 "check_ad": True if "yabs.yandex.ru" in self.waitAll(By.CSS_SELECTOR, ".path__item")[
                     el].get_attribute("href") else False}
            )

        if no_ad:
            common = []
            for el in result_list_value:
                if el["check_ad"]:
                    continue
                common.append(el)
            result_list_value = common

        return result_list_value
