from selenium.webdriver.common.by import By
from .BasePage import BasePage


class CommonElements(BasePage):

    def entry_field(self, text=""):
        entry_field = self.waitOne(By.CLASS_NAME, "input__control")
        if text != "":
            entry_field.send_keys(text)
        return entry_field

    def entry_field_value(self):
        return self.waitOne(By.CLASS_NAME, "input__control").get_attribute("value")

    def get_suggest_value(self):
        return self.waitOne(By.CLASS_NAME, "mini-suggest__popup-content"
                            ).find_elements_by_tag_name("li")
