from selenium.webdriver.common.by import By
from .BasePage import BasePage


class ImagePage(BasePage):

    def image_main_blocks(self, requested_section=0):

        block_obj = self.waitAll(By.CLASS_NAME, "PopularRequestList-SearchText")
        block_obj[requested_section].click()
        return block_obj[requested_section].text

    def circle_buttons(self, button: str):

        if button == "forward":
            self.waitOne(By.CSS_SELECTOR, ".CircleButton_type_next").click()
        elif button == "back":
            self.waitOne(By.CSS_SELECTOR, ".CircleButton_type_prev").click()

    def click_on_image(self, number=0):

        image = self.waitAll(By.CLASS_NAME, "serp-item__link")[number]
        image.click()
        return image

    def get_image_container(self):
        try:
            return self.waitOne(By.CLASS_NAME, "MMImageContainer")
        except:
            return False
