from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def waitAll(self, search, name):
        return WebDriverWait(self.driver, 15).until(
            EC.presence_of_all_elements_located(
                (search, name)
            )
        )

    def waitOne(self, search, name):
        return WebDriverWait(self.driver, 15).until(
            EC.presence_of_element_located(
                (search, name)
            )
        )
