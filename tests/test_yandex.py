from YandexPages import *

from Common import switch_window

from selenium.webdriver.common.keys import Keys

import pytest
import allure
import time


@allure.feature("Поиск в Яндексе")
def test_tensor_search_in_yandex(driver):
    driver.get("https://yandex.ru")

    results_page = ResultsPage(driver)
    common_elements = CommonElements(driver)

    assert common_elements.entry_field(), "Поле поиска не найдено"
    common_elements.entry_field("тензор")
    time.sleep(1)
    assert len(common_elements.get_suggest_value()) > 0, "Таблица с подсказками не обнаружена"
    common_elements.entry_field().send_keys(Keys.ENTER)
    result_list = results_page.return_result_list()
    assert 5 == len([x for x in result_list[0:5] if "tensor.ru" in x['result_link']]), f"Ожидалось, что первые 5 результатов содержат ссылку 'tensor.ru'"


@allure.feature("Картинки на Яндексе")
def test_yandex_image(driver):
    driver.get("https://yandex.ru")

    start_page = StartPage(driver)
    common_elements = CommonElements(driver)
    image_page = ImagePage(driver)

    start_page.click_on_yandex_service("Картинки")
    switch_window(driver, 1)
    time.sleep(2)
    assert "https://yandex.ru/images/" in driver.current_url, "Ожидалось 'https://yandex.ru/images/' в адресной строке"
    opened_block = image_page.image_main_blocks()
    assert opened_block in common_elements.entry_field().get_attribute(
        "value"), f"Открыли категорию {searched_image_text}, поле поиска содержит другое значение"
    image_page.click_on_image()
    assert image_page.get_image_container(), "Картинка не открылась"
    time.sleep(2)
    first_image = driver.current_url
    image_page.circle_buttons("forward")
    time.sleep(2)
    assert first_image != driver.current_url, "При нажатии кнопки вперед картинка не изменилась"
    image_page.circle_buttons("back")
    time.sleep(2)
    assert first_image == driver.current_url, "Изображение не равно изначальному"
