from selenium import webdriver
import pytest
from selenium.webdriver.support.events import EventFiringWebDriver, AbstractEventListener
import logging
from datetime import datetime as dt


@pytest.fixture(scope="session")
def driver(request):
    try:
        wd = webdriver.Remote(command_executor="http://selenium__standalone-firefox:4444/wd/hub",
        desired_capabilities={'browserName': 'firefox'})
        driver = EventFiringWebDriver(wd, MyListener(get_logger()))
    except:
        driver = EventFiringWebDriver(webdriver.Firefox(), MyListener(get_logger()))
    driver.implicitly_wait(10)
    request.addfinalizer(driver.quit)
    return driver



def get_logger():
    logger = logging.getLogger('selenium_logger')
    logger.setLevel(logging.DEBUG)
    ch = logging.FileHandler('protocol/logs/file.log')
    ch.setLevel(10)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.info("____INIT LOGGER_____ || testing begins")
    return logger


class MyListener(AbstractEventListener):

    def __init__(self, logger):
        self.logger = logger

    def before_find(self, by, value, driver):
        self.logger.info(f"Before find element '{value}' by '{by}'")

    def before_click(self, element, driver):
        self.logger.info(f"Before click on element '{element}'")

    def before_change_value_of(self, element, driver):
        self.logger.info(f"Before changing the value {element}")

    def on_exception(self, exception, driver):
        self.logger.error(f"Exception '{exception}' received")
        driver.save_screenshot(f'protocol/screenshots/{dt.now().strftime("%d.%m.%Y/%H-%M")}.png')
